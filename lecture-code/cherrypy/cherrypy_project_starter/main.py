#incomplete code
import cherrypy

# create movies.py, users.py, etc. in the curr dir
from movies import MovieController
from users import UserController
from votes import VoteController
from ratings import RatingController
from reset import ResetController

# copy your fully working python primer
from _movie_database import _movie_database

def start_service():
    dispatcher = cherrypy.dispatch.RoutesDispatcher()

    # instantiate mdb so that it is shared with all controllers

    mdb_o = _movie_database()

    # instantiate controllers
    movieController = MovieController(mdb=mdb_o)
    userController  = UserController(mdb=mdb_o)
    voteController  = VoteController(mdb=mdb_o)
    ratingController = RatingController(mdb=mdb_o)
    resetController = ResetController(mdb=mdb_o)

    #connecting endpoints

    #connect /movies/:movie_id resource
    dispatcher.connect('movie_get_mid', '/movies/:movie_id', controller=movieController, action='GET_MID', conditions=dict(method=['GET']))

    conf = {
        'global' : {
            'server.socket_host' : 'student04.cse.nd.edu',
            'server.socket_port' : 51002,
        },
        '/' : {
            'request.dipatch' : dispatcher,
        }
    }

    cherrypy.config.update(conf)
    app = cherrypy.tree.mount(None, config=conf)
    cherrypy.quickstart(app)


if __name__ == '__main__':
    start_service()
