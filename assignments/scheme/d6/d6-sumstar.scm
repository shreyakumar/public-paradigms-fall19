;; scheme daily homework
;; name: ??????????
;; date: ??????????


(define sum*
  (lambda (ttup)
    (cond
      ;; your code here
)))

;; tests!
(display (sum* '((5)) ))
(display "\n")

(display (sum* '((0) ((0) ((5))) ((0) ((10)))) ))
(display "\n")

(display (sum* '((0) ((0) ((5) ((7)))) ((0) ((10) ))) ))
(display "\n")

(display (sum* '((0) ((0) ((5) ((7) ) ((8) ))) ((0) ((10) ))) ))
(display "\n")

;; correct output:
;;   $ guile d6-sumstar.scm
;;   5
;;   15
;;   22
;;   30

